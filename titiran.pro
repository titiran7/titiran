# Add more folders to ship with the application, here
folder_01.source = qml/titiran
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    simulation.cpp \
    tuiodevice.cpp \
    pagestatus.h

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    simulation.h \
    tuiodevice.h \
    pagestatus.h

# Small configurations
CONFIG += precompile_header
win32:CONFIG += embed_manifest_exe
DEFINES += OSC_HOST_LITTLE_ENDIAN

# Windows libraries for OSC Pack
win32:LIBS += ws2_32.lib \
    winmm.lib

# External files for the project
include( 3rdparty.pri )

