#include "simulation.h"

#include <QGuiApplication>
#include <QTouchEvent>
#include <QWindow>

#include <QTimer>
#include <QDebug>

Simulation::Simulation(QObject *parent) :
    QObject(parent)
{
    touchDevice.setName("Dummy");
    touchDevice.setType(QTouchDevice::TouchScreen);
    touchDevice.setCapabilities(QTouchDevice::Position);
}

void Simulation::simulate()
{
    QTimer::singleShot(1000, this, SLOT(touch()));
}

void Simulation::touch()
{
    QList<QTouchEvent::TouchPoint> touchPoints;

    QTouchEvent::TouchPoint p(1);
    p.setState(Qt::TouchPointPressed);
    p.setPos(QPointF(360, 400));
    p.setPressure(0.5);

    touchPoints.append(p);

    QTouchEvent * touchEvent = new QTouchEvent(QEvent::TouchBegin, &touchDevice, Qt::NoModifier, Qt::TouchPointPressed, touchPoints);
    QGuiApplication::postEvent((QObject *)qApp->allWindows().at(0), touchEvent);
}
