#ifndef SIMULATION_H
#define SIMULATION_H

#include <QObject>
#include <QTouchDevice>

class Simulation : public QObject
{
    Q_OBJECT
public:
    explicit Simulation(QObject *parent = 0);
    void simulate();
    
public slots:
    void touch();

private:
    QTouchDevice touchDevice;

    
};

#endif // SIMULATION_H
