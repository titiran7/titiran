import QtQuick 2.0
import PageStatus 1.0

Item {
    id: root

    visible: false

    // Note we do not use anchor fill here because it will force us to relayout
    // hidden children when rotating the screen as well
    width: visible && parent ? parent.width - anchors.leftMargin - anchors.rightMargin : __prevWidth
    height: visible && parent ? parent.height  - anchors.topMargin - anchors.bottomMargin : __prevHeight
    x: parent ? anchors.leftMargin : 0
    y: parent ? anchors.topMargin : 0

    onWidthChanged: __prevWidth = visible ? width : __prevWidth
    onHeightChanged: __prevHeight = visible ? height : __prevHeight

    property int __prevWidth: 0
    property int __prevHeight: 0

    property bool __isPage: true

    anchors.margins: 0 // Page margins should generally be 16 pixels as defined by UI.MARGIN_XLARGE

    // The status of the page. One of the following:
    //      PageStatus.Inactive - the page is not visible
    //      PageStatus.Activating - the page is transitioning into becoming the active page
    //      PageStatus.Active - the page is the current active page
    //      PageStatus.Deactivating - the page is transitioning into becoming inactive
    property int status: PageStatus.Inactive

    // Defines the tools for the page; null for none.
    property Item tools: null

    // The page stack that the page is in.
    property PageStack pageStack
}
