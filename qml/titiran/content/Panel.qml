import QtQuick 2.0

Component {
    Item {
        id: page

        width: ListView.view.width
        height: ListView.view.height

        Rectangle{
            anchors.fill: parent
            color: "white"
            border.color: "black"
            border.width: 1

            Rectangle{
                radius: 16
                width: 30
                height: 30
                color: "black"
                anchors.right: label.left
                anchors.rightMargin: 5
                anchors.verticalCenter: label.verticalCenter
            }

            Text{
                id: label
                text: model.name
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.margins: 50
                font.pointSize: 30
            }
        }
    }
}
