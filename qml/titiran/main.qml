import QtQuick 2.0
import "content"

Rectangle {
    width: 320; height: 480

    ListModel {
        id: list

        ListElement{
            name: "Panel 1"
        }

        ListElement{
            name: "Panel 2"
        }

        ListElement{
            name: "Panel 3"
        }

    }

    ListView {
        id: flickable

        anchors.fill: parent
        focus: true
        highlightRangeMode: ListView.StrictlyEnforceRange
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        model: list
        delegate: Panel {}
    }
}
