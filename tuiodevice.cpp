#include "tuiodevice.h"

#include "TuioClient.h"
#include "TuioCursor.h"

#include <QGuiApplication>
#include <QWindow>

#include <QDebug>

TuioDevice::TuioDevice(QObject * parent) : QObject(parent), running(false), tuioClient(0){

    touchDevice.setName("TUIODevice");
    touchDevice.setType(QTouchDevice::TouchScreen);
    touchDevice.setCapabilities(QTouchDevice::Position);

    touchPointMap = new QMap<int, QTouchEvent::TouchPoint>();
}

TuioDevice::~TuioDevice(){

    stop();

    if(touchPointMap) delete touchPointMap;
    if(tuioClient) delete tuioClient;
}

// reimp
void TuioDevice::addTuioObject(TUIO::TuioObject *tobj)
{
    Q_UNUSED(tobj)
}

void TuioDevice::updateTuioObject(TUIO::TuioObject *tobj)
{
    Q_UNUSED(tobj)
}

void TuioDevice::removeTuioObject(TUIO::TuioObject *tobj)
{
    Q_UNUSED(tobj)
}

void TuioDevice::addTuioCursor(TUIO::TuioCursor *tcur)
{
    tuioToQt(tcur, QEvent::TouchBegin);
}

void TuioDevice::updateTuioCursor(TUIO::TuioCursor *tcur)
{
    tuioToQt(tcur, QEvent::TouchUpdate);
}

void TuioDevice::removeTuioCursor(TUIO::TuioCursor *tcur)
{
    tuioToQt(tcur, QEvent::TouchEnd);
}

void TuioDevice::refresh(TUIO::TuioTime ftime)
{
}

bool TuioDevice::tuioToQt(TUIO::TuioCursor *tcur, QEvent::Type eventType)
{

    QWindow * window = qApp->allWindows().at(0);

    const QPointF normPos(tcur->getX(), tcur->getY());
    const QPointF pos(window->width() * normPos.x(), window->height() * normPos.y());

    QTouchEvent::TouchPoint touchPoint(tcur->getSessionID());
    touchPoint.setPressure(1.0);
    touchPoint.setPos(pos);

    Qt::TouchPointStates touchPointStates;

    switch (eventType) {
    case QEvent::TouchBegin: {
        touchPointStates = Qt::TouchPointPressed;

        touchPoint.setState(Qt::TouchPointPressed);
        touchPoint.setStartPos(touchPoint.pos());
        touchPoint.setLastPos(touchPoint.pos());
        touchPointMap->insert(tcur->getSessionID(), touchPoint);

        break;
    }
    case QEvent::TouchUpdate: {
        if (tcur->getMotionSpeed() > 0){
            touchPointStates = Qt::TouchPointMoved;
            touchPoint.setState(Qt::TouchPointMoved);
        }else{
            touchPointStates = Qt::TouchPointStationary;
            touchPoint.setState(Qt::TouchPointStationary);
        }

        touchPoint.setStartPos(touchPointMap->value(tcur->getSessionID()).startPos());
        touchPoint.setLastPos(touchPointMap->value(tcur->getSessionID()).pos());
        touchPointMap->insert(tcur->getSessionID(), touchPoint);

        break;
    }
    case QEvent::TouchEnd: {

        touchPointStates = Qt::TouchPointReleased;
        touchPoint.setState(Qt::TouchPointReleased);
        touchPoint.setStartPos(touchPointMap->value(tcur->getSessionID()).startPos());
        touchPoint.setLastPos(touchPointMap->value(tcur->getSessionID()).pos());
        touchPointMap->insert(tcur->getSessionID(), touchPoint);

        break;
    }
    default: break;
    }

    // construct an event
    QTouchEvent * event = new QTouchEvent(
                eventType,
                &touchDevice,
                Qt::NoModifier,
                touchPointStates,
                touchPointMap->values()
                );

    propagateEvent(event);

    if (eventType == QEvent::TouchEnd) touchPointMap->remove(tcur->getSessionID());

    return true;
}

void TuioDevice::propagateEvent(QEvent *e)
{
    QGuiApplication::postEvent((QObject *)qApp->allWindows().at(0), e);
}

void TuioDevice::start()
{
    if(!tuioClient) tuioClient = new TUIO::TuioClient(3333);
    tuioClient->addTuioListener(this);
    tuioClient->connect();
    running = true;
}

void TuioDevice::stop()
{
    if(running){
        tuioClient->disconnect();
        tuioClient->removeTuioListener(this);
        running = false;
    }
}

