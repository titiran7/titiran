#include <QtGui/QGuiApplication>
#include <QQmlComponent>
#include "qtquick2applicationviewer.h"


#include "simulation.h"
#include "tuiodevice.h"
#include "pagestatus.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QtQuick2ApplicationViewer viewer;
    viewer.setMainQmlFile(QStringLiteral("qml/titiran/main.qml"));
    viewer.showExpanded();

    // Simulation sim;
    // sim.simulate();

    TuioDevice device;
    device.start();

    qmlRegisterType<PageStatus>("PageStatus", 1,0, "PageStatus");


    return app.exec();
}
