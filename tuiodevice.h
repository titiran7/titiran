#ifndef TUIODEVICE_H
#define TUIODEVICE_H

#include <QObject>
#include <QTouchEvent>
#include <QTouchDevice>
#include "./3rdparty/tuio/TuioListener.h"

namespace TUIO {
    class TuioClient;
    class TuioCursor;
}

class TuioDevice : public QObject, public TUIO::TuioListener
{

public:
    explicit TuioDevice(QObject * parent = 0);
    ~TuioDevice();

    virtual void addTuioObject(TUIO::TuioObject *tobj);
    virtual void updateTuioObject(TUIO::TuioObject *tobj);
    virtual void removeTuioObject(TUIO::TuioObject *tobj);
    virtual void addTuioCursor(TUIO::TuioCursor *tcur);
    virtual void updateTuioCursor(TUIO::TuioCursor *tcur);
    virtual void removeTuioCursor(TUIO::TuioCursor *tcur);
    virtual void refresh(TUIO::TuioTime ftime);

    void start();
    void stop();

private:
    bool tuioToQt(TUIO::TuioCursor *tcur, QEvent::Type eventType);
    TUIO::TuioClient *tuioClient;
    QMap<int, QTouchEvent::TouchPoint> * touchPointMap;

    QTouchDevice touchDevice;

    bool running;

    void propagateEvent(QEvent * e);

};
#endif // TUIODEVICE_H
